# frozen_string_literal: true

module RemoteDevelopment
  module Workspaces
    module Reconcile
      module UpdateType
        PARTIAL = 'partial'
        FULL = 'full'
      end
    end
  end
end
